#include "type_embarcation.h"
#include <iostream>

Type_embarcation::Type_embarcation():
  code(""),
  nom(""),
  nb_place(0),
  prix_demi_heure(0, 0),
  prix_heure(0, 0),
  prix_demi_jour(0, 0),
  prix_jour(0, 0)

  Type_embarcation::Type_embarcation(std::string _code,
                                     std::string _nom,
                                     unsigned int _nb_place,
                                     std::decimal _prix_demi_heure,
                                     std::decimal _prix_heure,
                                     std::decimal _prix_demi_jour,
                                     std::decimal _prix_jour):
  code(_code),
  nom(_nom),
  nb_place(_nb_place),
  prix_demi_heure(_prix_demi_heure),
  prix_heure(_prix_heure),
  prix_demi_jour(_prix_demi_jour),
  prix_jour(_prix_jour);
{}


Type_embarcation::~Type_embarcation();

std::string Type_embarcation::getCode()
{
  return code;
}

std::string Type_embarcation::getNom()
{
  return nom;
}

void Type_embarcation::setNom(std::string _nom)
{
  nom = _nom;
}


unsigned int Type_embarcation::getNb_place()
{
  return nb_place;
}

void Type_embarcation::setNb_place(unsigned int _nb_place)
{
  nb_place = _nb_place;
}

std::decimal Type_embarcation::getPrix_demi_heure()
{
  return prix_demi_heure;
}


void Type_embarcation::setPrix_demi_heure(std::decimal _prix_demi_heure)
{
  prix_demi_heure = _prix_demi_heure;
}


std::decimal Type_embarcation::getPrix_heure()
{
  return prix_heure;
}

void Type_embarcation::setPrix_heure(std::decimal _prix_heure)
{
  prix_heure = _prix_heure;
}


std::decimal Type_embarcation::getPrix_demi_jour()
{
  return prix_demi_jour;
}

void Type_embarcation::setPrix_demi_jour(std::decimal _prix_demi_jour)
{
  prix_demi_jour = _prix_demi_jour;
}

std::decimal Type_embarcation::getPrix_jour()
{
  return prix_jour;
}

void Type_embarcation::setPrix_jour(std::decimal _prix_jour)
{
  prix_jour = _prix_jour;
}
