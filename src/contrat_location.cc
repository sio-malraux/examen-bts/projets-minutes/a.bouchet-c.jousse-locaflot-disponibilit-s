#include "contrat_location.h"
#include <chrono>
#include <iostream>

Contrat_location::Contrat_location():
  id(0),
{
  date.std::chrono::system_clock::now();
  std::chrono::time_point<std::chrono::system_clock> heure_debut, heure_fin;
  heure_debut = std::chrono::system_clock::now();
  heure_fin = std::chrono::system_clock::now();
}
  Contrat_location::Contrat_location(unsigned int _id,
                                     std::chrono::system_clock _date,
                                     std::chrono::time_point<std::chrono::system_clock> _heure_D,
                                     std::chrono::time_point<std::chrono::system_clock> _heure_F):
    id(_id),
    date(_date),
    heure_debut(_heure_D),
    heure_fin(_heure_F)
  {}

Contrat_location::~Contrat_location() {}

unsigned int Contrat_location::getId()
{
  return id;
}

std::chrono::system_clock Contrat_location::getDate()
{
  return date;
}

std::chrono::time_point<std::chrono::system_clock> Contrat_location::getHeure_debut()
{
  return heure_debut;
}

std::chrono::time_point<std::chrono::system_clock> Contrat_location::getHeure_fin()
{
  return heure_fin;
}

void Contrat_location::setDate(std::chrono::system_clock _date)
{
  date = _date;
}

void Contrat_location::setHeure_debut(std::chrono::time_point<std::chrono::system_clock> _heure_D)
{
  heure_debut = _heure_D;
}

void Contrat_location::setHeure_fin(std::chrono::time_point<std::chrono::system_clock> _heure_F)
{
  heure_fin = _heure_F;
}

  
