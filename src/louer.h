#ifndef LOUER_H
#define LOUER_H
#include <iostream>

/**
 * \class Louer
 * \brief Definition of the class Louer
 */
class Louer
{
public:

  /**
   * \brief Default Constructor of Louer
   */
  Louer();
  
  /**
   * \brief Constructor with parameters
   * \param[in] id the identification number of an Louer
   * \param[in] id_embarcation the foreign key of an Embarcation
   * \param[in] nb_personnes number of people of an Embarcation
   */
  Louer(unsigned int, std::string, unsigned int);

  /**
   * \brief Default Destructor of Louer
   */
  ~Louer();

  /**
   * \brief Lets you know the ID of an louer
   * \return unsigned int represents the ID of an louer
   */
  unsigned int getId();

  /**
   * \brief Lets you know the ID of an embarcation
   * \return std::string represents the ID of an embarcation
   */
  std::string getId_embarcation();

  /**
   * \brief Lets you know the number of people of an louer
   * \return unsigned int represents the number of people of an louer
   */
  unsigned int getNb_personnes();

  /**
   * \brief to set the number of people of an louer
   */
  void setNb_personnes(unsigned int);


private:
  unsigned int id;///< Attribute defining the ID of an louer
  std::string id_embarcation;///< Attribute defining the ID of an embarcation
  unsigned int nb_personnes;///< Attribute defining the number of people of an louer
};

#endif // LOUER_H
