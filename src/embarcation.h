#ifndef EMBARCATION_H
#define EMBARCATION_H
#include <iostream>
#include <boolean>

/**
 * \class Embarcation
 * \brief  Definition of the class Embarcation
 */
class Embarcation
{
  public:

  /**
   * \brief Default Constructor of Embarcation
   */
  Embarcation();

  /**
   * \brief Constructor with parameters
   * \param[in] id the identification number of an Embarcation
   * \param[in] couleur color of an Embarcation
   * \param[in] type type of an Embarcation
   * \param[in] disponible release available of an Embarcation
   */
  Embarcation(unsigned int, std::string, std::string, std::boolean);

  /**
   * \brief Default Destructor of Embarcation
   */
  ~Embarcation();

  /**
   * \brief Lets you know the ID of an Embarcation
   * \return unsigned int represents the ID of an Embarcation
   */
  unsigned int getId();

  /**
   * \brief Lets you know the color of an Embarcation
   * \return std::string represents the color of an Embarcation
   */
  std::string getCouleur();

  /**
   * \brief to set the color of an Embarcation
   */
  void setCouleur(std::string);

  /**
   * \brief Lets you know the type of an Embarcation
   * \return std::string represents the type of an Embarcation
   */
  std::string getType();

  /**
   * \brief to set the type of an Embarcation
   */
  void setType(std::string);

  /**
   * \brief Lets you know the available of an Embarcation
   * \return std::boolean represents the available of an Embarcation
   */
  std::boolean getDisponible();

  /**
   * \brief to set the available of an Embarcation
   */
  void setDisponible(std::boolean);

private:
  unsigned int id;///< Attribute defining the ID of an Embarcation
  std::string couleur;///< Attribute defining the color of an Embarcation
  std::string type;///< Attribute defining the type of an Embarcation
  std::boolean disponible;///< Attribute defining the available of an Embarcation
};

#endif // EMBARCATION_H
