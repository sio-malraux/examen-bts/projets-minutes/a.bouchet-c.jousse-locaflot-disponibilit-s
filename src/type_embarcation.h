#ifndef TYPE_EMBARCATION_H
#define TYPE_EMBARCATION_H
#include <iostream>
#include <decimal>

/**
 * \class Type_embarcation
 * \brief Definition of the class Type_embarcation
 */
class Type_embarcation
{
public:
  /**
   * \brief Default Constructor of Type_embarcation
   */
  Type_embarcation();

  /**
   * \brief Constructor with parameters
   * \param[in] code the identification of an type_embarcation
   * \param[in] nom name of an type_embarcation
   * \param[in] nb_place number of places of an type_embarcation
   * \param[in] prix_demi_heure price for half an hour of an type_embarcation
   * \param[in] prix_heure price for hour of an type_embarcation
   * \param[in] prix_demi_jour price for half an day of an type_embarcation
   * \param[in] prix_jour price for day of an type_embarcation
   */
  Type_embarcation(std::string,
                   std::string,
                   unsigned int,
                   std::decimal,
                   std::decimal,
                   std::decimal,
                   std::decimal);

  /**
   * \brief Deafault Destructor of Type_embarcation
   */
  ~Type_embarcation();

  /**
   * \brief Lets you know the code of an type_embarcation
   * \return std::string represents the CODE of an type_embarcation
   */
  std::string getCode();

  /**
   * \brief Lets you know the name of an type_embarcation
   * \return std::string represents the name of an type_embarcation
   */
  std::string getNom();


  /**
   * \brief to set the name of an type_embarcation
   */
  void setNom(std::string);

  /**
   * \brief Lets you know the number of places of an type_embarcation
   * \return unsigned int represents the number of an type_embarcation
   */
  unsigned int getNb_place();

  /**
   * \brief to set the number of places of an type_embarcation
   */
  void setNb_place(unsigned int);

  /**
   * \brief Lets you know the price for half an hour of an type_embarcation
   * \return std::decimal represents the price for half an hour of an type_embarcation
   */
  std::decimal getPrix_demi_heure();

  /**
   * \brief to set the price for half an hour of an type_embarcation
   */
  void setPrix_demi_heure(std::decimal);

  /**
   * \brief Lets you know the price for hour of an type_embarcation
   * \return std::decimal represents the price for hour of an type_embarcation
   */
  std::decimal getPrix_heure();

  /**
   * \brief to set the price for hour of an type_embarcation
   */
  void setPrix_heure(std::decimal);

  /**
   * \brief Lets you know the price for half day of an type_embarcation
   * \return std::decimal represents the price for half day of an type_embarcation
   */
  std::decimal getPrix_demi_jour();

  /**
   * \brief to set the price half day of an type_embarcation
   */
  void setPrix_demi_jour(std::decimal);

  /**
   * \brief Lets you know the price for day of an type_embarcation
   * \return std::decimal represents the price for day of an type_embarcation
   */
  std::decimal getPrix_jour();

  /**
   * \brief to set the price for day of an type_embarcation
   */
  void setPrix_jour(std::decimal);

private:
  std::string code;///< Attribute defining the CODE of an type_embarcation
  std::string nom;///< Attribute defining the name of an type_embarcation
  unsigned int nb_place;///< Attribute defining the number of places of an type_embarcation
  std::decimal prix_demi_heure;///< Attribute defining the price for half an hour of an type_embarcation
  std::decimal prix_heure;///< Attribute defining the price for hour of an type_embarcation
  std::decimal prix_demi_jour;///< Attribute defining the half day of an type_embarcation
  std::decimal prix_jour;///< Attribute defining day of an type_embarcation
};

#endif // EMBARCATION_H
