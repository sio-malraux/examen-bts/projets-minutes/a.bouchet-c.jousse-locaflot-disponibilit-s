#ifndef CONTRAT_LOCATION_H
#define CONTRAT_LOCATION_H
#include <iostream>
#include <chrono>

/**
 * \class Contrat_location
 * \brief Definition of the class Contrat_location
 */
class Contrat_location
{
public :

  /**
   * \brief Default Constructor of Contrat_location
   */
  Contrat_location();

  /**
   * \brief Constructor with parameters
   * \param[in] id the identification number of an Contrat_location
   * \param[in] date release date of an Contrat_location
   * \param[in] heure_debut release beginning time of an Contrat_location
   * \param[in] heure_fin release end time of an Contrat_location
   */
  Contrat_location(unsigned int, std::chrono::system_clock, std::chrono::time_point<std::chrono::system_clock>, std::chrono::time_point<std::chrono::system_clock>);


  /**
   * \brief Default Destructor of Contrat_location
   */
  ~Contrat_location();

  /**
   * \brief Lets you know the ID of an Contrat_location
   * \return unsigned int represents the ID of an Contrat_location
   */
  unsigned int getId();

  /**
   * \brief Lets you know the date of an Contrat_location
   * \return system_clock represents the date of an Contrat_location
   */
  std::chrono::system_clock getDate();

  /**
   * \brief to set the date of an Contrat_location
   */
  void setDate(std::chrono::system_clock);

  /**
   * \brief Lets you know the beginning time of an Contrat_location
   * \return time_point(system_clock) reprents the beginning time of an Contrat_location
   *\
  std::chrono::time_point<std::chrono::system_clock> getHeure_debut();

  /**
   * \brief to set the beginning time of an Contrat_location
   */
  void setHeure_debut(std::chrono::time_point<std::chrono::system_clock>);


  /**
   * \brief Lets you know the end time of an Contrat_location
   * \return time_point(system_clock) reprents the end time of an Contrat_location
   */
  std::chrono::time_point<std::chrono::system_clock> getHeure_fin();


  /**
   * \brief to set the end time of an Contrat_location
   */
  void setHeure_fin(std::chrono::time_point<std::chrono::system_clock>);

private:
  unsigned int id;///< Attribute definig the ID of an Contrat_location
  std::chrono::system_clock date;///< Attribute determining the release date of an Contrat_location
  std::chrono::time_point<std::chrono::system_clock> heure_debut;///< Attribute determining the release beginning time of an Contrat_location 
  std::chrono::time_point<std::chrono::system_clock> heure_fin;///< Attribute determining the release end time of an Contrat_location
};

#endif // CONTRAT_LOCATION_H
  
