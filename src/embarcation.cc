#include "embarcation.h"
#include <iostream>

Embarcation::Embarcation():
  id(0),
  couleur(""),
  type(""),
  disponible(false)

  Embarcation::Embarcation(unsigned int _id,
                           std::string _couleur,
                           std::string _type,
                           std::boolean _disponible):

  id(_id),
  couleur(_couleur),
  type(_type),
  disponible(_disponible)
{}

Embarcation::~Embarcation() {}

unsigned int Embarcation::getId()
{
  return id;
}

std::string Embarcation::getCouleur()
{
  return couleur;
}

void Embarcation::setCouleur(std::string _couleur)
{
  couleur = _couleur;
}

std::string Embarcation::getType()
{
  return type;
}

void Embarcation::setType(std::string _type)
{
  type = _type;
}

std::boolean Embarcation::getDisponible()
{
  return disponible;
}

void Embarcation::setDisponible(std::boolean _disponible)
{
  disponible = _disponible;
}
