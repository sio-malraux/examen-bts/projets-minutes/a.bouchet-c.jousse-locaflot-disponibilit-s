#include "louer.h"
#include <iostream>

Louer::Louer():
  id(0),
  id_embarcation(""),
  nb_personnes(0)

  Louer::Louer(unsigned int _id,
               std::string _id_embarcation,
               unsigned int _nb_peronnes):

  id(_id),
  id_embarcation(_id_embarcation),
  nb_personnes(_nb_personnes)
{}

Louer::~Louer() {}

unsigned int Louer::getId()
{
  return id;
}

std::string Louer::getId_embarcation()
{
  return id_embarcation;
}

unsigned int Louer::getNb_personnes()
{
  return nb_personnes;
}
void Louer::setNb_personnes(unsigned int _nb_personnes)
{
  nb_personnes = _nb_personnes;
}
